import socket
import select
import os
import sys
import time
from modules import msvcrt
import json


class Client(object):

    @staticmethod
    def clear_terminal():
        os.system("clear")

    def __init__(self):
        # Clear the terminal
        Client.clear_terminal()
        self.my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.my_socket.connect(('127.0.0.1', 23))
        self.messages_to_send = []
        """ :param A list containing the messages that are needed to be sent. """
        self.admin = False
        """ :param A boolean representing whether this client is and admin user or not. """
        self.commands = {'ma': 2, 'rm': 3, 'mt': 4, 'pr': 5, 'va': 6, 'um': 7, 'ra': 8}
        """ :param A dictionary of command numbers {command: command_number}. """
        self.rlist, self.wlist, self.xlist = select.select([self.my_socket], [self.my_socket], [])
        """ :param Lists representing the sockets you can read from, write to and error sockets. """
        self.username = self.make_username()
        """ :param A string representing the username of the client. """
        self.return_line = True
        """ :param A boolean representing whether to return to start of line or not. This is needed because if the 
        client gets two consecutive messages from the server we want them to be in separate lines. """
        self.print_help_message()

    def show_username(self):
        # TODO: Ask Tsarfati if it should be the username or '>'.
        """
        Shows the client's username using stdout. This is used to display the client's username as he types.
        """
        if self.admin:
            sys.stdout.write('@' + self.username + ': ')
        else:
            sys.stdout.write(self.username + ': ')

    def print_help_message(self):
        """
        Prints the a message about how to use the chat and the commands that exist. The message changes depending on
        whether you're an admin or not.
        """
        apple_logo = u'\uf8ff'
        print '\nWelcome to iCh' + apple_logo + 't!!!\n'
        if self.admin:
            print '>If you want to send a regular message enter the message and press the enter key at the end.\n' \
                  '>If you would like to perform something more complicated here is the list of available commands:' \
                  '\n\n1. ma <username>: Makes <username> admin.\n2. ra <username>: Removes admin permissions from ' \
                  '<username>\n3. rm <username>: Removes <username>.\n4. mt <username>: Mutes <username>.\n5. um ' \
                  '<username>: Unmutes <username>.\n6. pr <username> <message>: Privately sends <message> to ' \
                  '<username>.\n7. va: Displays a list of admins.\n8. help: Displays the commands menu.\n9. quit: ' \
                  'Quits the chat.\n'
        else:
            print '>If you want to send a regular message enter the message and press the enter key at the end.\n' \
                  '>If you would like to perform something more complicated here is the list of available commands:' \
                  '\n\n1. pr <username> <message>: Privately sends <message> to <username>.\n' \
                  '2. va: Displays a list of admins.\n3. help: Displays the command menu\n4. quit: Quits the chat.\n'

    def make_username(self):
        """
        Asks the user for a username and sends it to the server to get validation.
        :return: If successful a string representing the username, if not nothing.
        """
        while True:
            username = raw_input('Enter your username: ')
            self.my_socket.send('VALIDATE' + username)
            response = self.my_socket.recv(1024)
            if response != 'False':
                # The validation was successful
                if response.startswith('ADMIN'):
                    # Logged in as an admin user
                    self.admin = True
                    username = response.split('ADMIN')[1]
                    print '>You have successfully created an admin user!\n'
                else:
                    # Logged in as a regular user
                    print '>You have successfully created a user!\n'
                # Give the user a little time to read the success message
                time.sleep(1)
                # Clear the terminal
                Client.clear_terminal()
                return username
            else:
                # The validation was not successful
                print '>It seems you have entered an invalid username.\n>Note that usernames cannot be "server", ' \
                      'cannot start with a "@", cannot include white spaces and must be unique!\n'

    def send_messages(self):
        """
        Send all waiting messages to server.
        """
        for msg in self.messages_to_send:
            # Find the command number
            if msg.lower() == 'quit' or (msg.lower().startswith('quit') and msg.lower().split('quit', 1)[1].isspace()):
                command_number = 0
                params = ''
            elif msg[:2].lower() not in self.commands:
                # The user sends a regular message and not a command
                command_number = 1
                params = msg
            else:
                # Get the command number from the commands dictionary
                command_number = self.commands[msg[:2].lower()]
                # Everything else is additional command parameters
                params = msg[3:]
            # Send username + command number + additional parameters
            data = {'username': self.username, 'command_number': command_number, 'params': params}
            json_data = json.dumps(data)
            self.my_socket.send(json_data)
            self.messages_to_send.remove(msg)

    @staticmethod
    def exit_program():
        """
        Prints a message and exits the program.
        """
        print '\rConnection with server closed'
        exit(1)

    def update_socket_status(self):
        """
        Updates the socket's status.
        """
        self.rlist, self.wlist, self.xlist = select.select([self.my_socket], [self.my_socket], [])

    def try_to_read_from_server(self):
        """
        Tries to read data from server and handles the received data.
        """
        if self.my_socket in self.rlist:
            if self.return_line:
                # Return to start of the line
                sys.stdout.write('\r')
                self.return_line = False
            # Read data from server
            data = self.my_socket.recv(1024)
            if data:
                if data[6:].startswith('@SERVER: Administration granted!'):
                    # It is a message from the server that makes the client admin
                    self.admin = True
                    print data
                    self.print_help_message()
                    self.show_username()
                elif data[6:].startswith('@SERVER: Administration removed!'):
                    # It is a message from the server that makes the client a regular user
                    self.admin = False
                    print data
                    self.print_help_message()
                    self.show_username()
                else:
                    print data
                    self.show_username()
            else:
                self.exit_program()

    def try_to_write_to_server(self):
        """
        Tries to write data to server.
        """
        if self.my_socket in self.wlist:
            self.send_messages()

    def handle_socket(self):
        """
        Handles the socket logic
        """
        self.return_line = True
        self.update_socket_status()
        self.try_to_read_from_server()
        self.try_to_write_to_server()

    def get_input(self):
        """
        Gets user input
        """
        message = msvcrt.getch()
        if message:
            if message.lower() == 'help' or (
                    message.lower().startswith('help') and message.lower().split('help', 1)[1].isspace()):
                self.print_help_message()
            else:
                self.messages_to_send.append(message)


def main():
    """
    Creates a new Client and handles the client logic
    """
    client = Client()
    while True:
        # Show client username as he types
        client.show_username()
        while not msvcrt.kbhit():
            # There is no keyboard event happening
            client.handle_socket()

        while msvcrt.kbhit():
            # There is a keyboard event happening
            client.get_input()


if __name__ == '__main__':
    main()
