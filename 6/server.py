import select
import socket
from datetime import datetime
import os
import json


class Server(object):

    @staticmethod
    def clear_terminal():
        os.system("clear")

    def __init__(self):
        # Clear terminal
        Server.clear_terminal()
        print 'Server Up...'
        self.server_socket = socket.socket()
        self.server_socket.bind(('0.0.0.0', 23))
        self.server_socket.listen(5)
        self.open_client_sockets = []
        """ :param A list of all client sockets of registered users the server has made connection with """
        self.pending_client_sockets = []
        """ :param A list of all connected client sockets whose users haven't been registered yet """
        self.client_message_dict = {}
        """ :param A dictionary of tuples {client_username: (SocketObject, [messages_to_be_sent])} """
        self.admin_usernames = ['daniel']
        """ :param A list containing the admin usernames """
        self.muted_users = []
        """ :param A list containing the usernames of the muted users """
        self.exceptions = {
            'muted': 'You cannot write in this chat due to the fact that you have been muted!',
            'non_existing_user': 'Command rejected! this user does not exist!',
            'self_command': 'Command rejected! cannot perform command on yourself!',
            'permission_denied': 'Permission denied! you do not have permissions to execute this command due to the '
                                 'fact that you are not an admin.',
            'invalid_message': 'This message is invalid! messages must not be empty!',
            'invalid_syntax': 'Invalid syntax! try putting a whitespace between each parameter. For more information '
                              'use the "help" command.',
            'existing_admin': 'Command rejected! this user is already an admin!',
            'non_existing_admin': 'Command rejected! this user is not an admin!',
            'muted_user': 'Command rejected! this user is already muted!',
            'unmuted_user': 'Command rejected! this user is already unmuted!',
        }
        """ :param A dictionary containing the exceptions and their messages """
        self.rlist, self.wlist, self.xlist = select.select(
            [self.server_socket] + self.open_client_sockets + self.pending_client_sockets,
            self.open_client_sockets + self.pending_client_sockets, [])
        """ :param Lists representing the sockets you can read from, write to and error sockets. """

    def do_commands(self, data):
        """
        Extract username, command number and parameters and call a command with them.
        :param data: A json string representing the data received from the client.
        """
        dict_data = json.loads(data)
        username = dict_data['username']
        command_number = dict_data['command_number']
        params = dict_data['params']

        # Call each command with parameters needed
        if command_number == 0:
            self.disconnect_client(username, 'has left the chat!')
        elif command_number == 1:
            self.normal_send(username, params)
        elif command_number == 2:
            self.make_admin(username, params)
        elif command_number == 3:
            self.remove_user(username, params)
        elif command_number == 4:
            self.mute_user(username, params)
        elif command_number == 6:
            self.view_admins(username, params)
        elif command_number == 7:
            self.unmute_user(username, params)
        elif command_number == 8:
            self.remove_admin(username, params)
        else:
            self.send_private_message(username, params)

    def append_message(self, username, message, receiver=None, server_message=False):
        """
        Appends the message to the clients' messages list.
        :param username: The username of the client who sent the message.
        :param message: The message to be sent.
        :param receiver: a string representing the username of the client the message will be sent to, if None sends
        to everyone except the client who sent the message.
        :param server_message: a boolean representing whether or not this is a message from the server.
        """
        time = datetime.now().strftime('%H:%M')
        if server_message:
            data = time + ' ' + username + ' ' + message
        else:
            if username in self.admin_usernames:
                data = time + ' ' + '@' + username + ': ' + message
            else:
                data = time + ' ' + username + ': ' + message
        if receiver:
            # Send the message only to the receiver
            receiver_messages_list = self.client_message_dict[receiver][1]
            receiver_messages_list.append(data)
        else:
            # Send message to everyone
            for client_username in self.client_message_dict:
                if client_username != username:
                    self.client_message_dict[client_username][1].append(data)

    def normal_send(self, username, message):
        """
        Sends all the clients' messages.
        :param username: The username of the client who sent the message.
        :param message: The message the client wants to send.
        """
        if username in self.muted_users:
            self.send_exception(username, 'muted')
        else:
            self.append_message(username, message)

    def server_send(self, username, message, receiver=None):
        """
        Sends all the server special messages.
        :param username: The username of the client who sent the message.
        :param message: The message to be sent.
        :param receiver: a string representing the username of the client the message will be sent to, if None sends
        to everyone except the client who sent the message.
        """
        self.append_message(username, message, receiver=receiver, server_message=True)

    def make_admin(self, username, user):
        """
        Makes the specified user an admin.
        :param username: The username of the client who requested this command.
        :param user: The username of the user to be made admin.
        """
        if username.startswith(' '):
            self.send_exception(username, 'invalid_syntax')
        elif username not in self.admin_usernames:
            self.send_exception(username, 'permission_denied')
        elif user not in self.client_message_dict:
            self.send_exception(username, 'non_existing_user')
        elif user in self.admin_usernames:
            self.send_exception(username, 'existing_admin')
        else:
            self.admin_usernames.append(user)
            self.server_send('@SERVER:', 'Administration granted! {} has made you admin!'.format(username),
                             receiver=user)
            self.server_send('@SERVER:', 'You have successfully made {} admin!'.format(user), receiver=username)
            print 'Successfully made {} admin!'.format(user)

    def remove_admin(self, username, user):
        """
        Removes admin permissions from the specified user.
        :param username: The username of the client who requested this command.
        :param user: The username of the user to be removed admin permissions from.
        """
        if username.startswith(' '):
            self.send_exception(username, 'invalid_syntax')
        elif username not in self.admin_usernames:
            self.send_exception(username, 'permission_denied')
        elif user not in self.client_message_dict:
            self.send_exception(username, 'non_existing_user')
        elif user not in self.admin_usernames:
            self.send_exception(username, 'non_existing_admin')
        else:
            self.admin_usernames.remove(user)
            self.server_send('@SERVER:',
                             'Administration removed! {} has taken admin permissions from you!'.format(username),
                             receiver=user)
            self.server_send('@SERVER:', 'You have successfully removed admin permissions from {}!'.format(user),
                             receiver=username)
            print 'Successfully removed admin permissions from {}!'.format(user)

    def remove_user(self, username, user):
        """
        Removes a user.
        :param username: The username of the client who requested this command.
        :param user: The username of the user to be removed.
        """
        if username not in self.admin_usernames:
            self.send_exception(username, 'permission_denied')
        elif user.startswith(' '):
            self.send_exception(username, 'invalid_syntax')
        elif user not in self.client_message_dict:
            self.send_exception(username, 'non_existing_user')
        elif user == username:
            self.send_exception(username, 'self_command')
        else:
            self.server_send('@SERVER:', 'You have removed {}.'.format(user), receiver=username)
            self.disconnect_client(user, disconnection_message='has been kicked from the chat!')
            print 'Removed {}.'.format(user)

    def mute_user(self, username, user):
        """
        Mutes a user.
        :param username: The username of the client who requested this command.
        :param user: The username of the user to be muted.
        """
        if username not in self.admin_usernames:
            self.send_exception(username, 'permission_denied')
        elif user not in self.client_message_dict:
            self.send_exception(username, 'non_existing_user')
        elif user == username:
            self.send_exception(username, 'self_command')
        elif user in self.muted_users:
            self.send_exception(username, 'muted_user')
        else:
            self.muted_users.append(user)
            self.server_send('@SERVER:', 'You have muted {}.'.format(user), receiver=username)
            self.server_send('@SERVER:', 'You have been muted, you cannot write messages anymore.',
                             receiver=user)
            print 'Muted {}.'.format(user)

    def unmute_user(self, username, user):
        """
        Unmutes a user.
        :param username: The username of the client who requested this command.
        :param user: The username of the user to be muted.
        """
        if username not in self.admin_usernames:
            self.send_exception(username, 'permission_denied')
        elif user not in self.client_message_dict:
            self.send_exception(username, 'non_existing_user')
        elif user == username:
            self.send_exception(username, 'self_command')
        elif user not in self.muted_users:
            self.send_exception(username, 'unmuted_user')
        else:
            self.muted_users.remove(user)
            self.server_send('@SERVER:', 'You have unmuted {}.'.format(user), receiver=username)
            self.server_send('@SERVER:', 'You have been unmuted, you can now write messages again.',
                             receiver=user)
            print 'Unmuted {}.'.format(user)

    def send_private_message(self, username, data):
        """
        Sends a private message to a user.
        :param username: The username of the client who requested this command.
        :param data: The user the message will be sent to and the message itself .
        """
        if ' ' not in data:
            self.send_exception(username, 'invalid_syntax')
        else:
            user, message = data.split(' ', 1)
            if username in self.muted_users:
                self.send_exception(username, 'muted')
            elif user not in self.client_message_dict:
                self.send_exception(username, 'non_existing_user')
            elif user == username:
                self.send_exception(username, 'self_command')
            elif not message or message.isspace():
                self.send_exception(username, 'invalid_message')
            else:
                self.append_message('!' + username, message, receiver=user)

    def view_admins(self, username, data):
        """
        Sends the list of admins.
        :param username: The username of the user who requested this command.
        :param data: Additional data the user might have sent.
        """
        if not data or data.isspace():
            formatted_list = '\n- ' + '\n- '.join(self.admin_usernames)
            self.server_send('@SERVER:', formatted_list, receiver=username)
        else:
            self.send_exception(username, 'invalid_syntax')

    def send_exception(self, username, exception):
        """
        Sends a permission denied message to a user.
        :param username: The username of the user to send the message to.
        :param exception: The type of the exception.
        """
        exception_message = self.exceptions[exception]
        self.server_send('@SERVER:', exception_message, receiver=username)

    def disconnect_client(self, username, disconnection_message):
        """
        Disconnects the client from the server and sends a message to all the other clients that this client has
        disconnected.
        :param username: The username of the client who will be disconnected.
        :param disconnection_message: The disconnection message that will be sent to the clients.
        """
        # Find the needed client socket
        client_socket = [self.client_message_dict[client_username][0] for client_username in self.client_message_dict if
                         client_username == username][0]
        # Close connection
        client_socket.close()
        # The client has disconnected
        print 'Connection with client {} closed'.format(username)
        # Remove the client and it's messages from the dictionary from the open_client_sockets_list, and if he is an
        # admin from the admin list.
        del self.client_message_dict[username]
        self.open_client_sockets.remove(client_socket)
        # Send the message about the disconnection to everyone
        self.server_send(username, disconnection_message)

    def update_sockets_status(self):
        """
        Update the sockets' state.
        """
        self.rlist, self.wlist, self.xlist = select.select(
            [self.server_socket] + self.open_client_sockets + self.pending_client_sockets,
            self.open_client_sockets + self.pending_client_sockets, [])

    def validate_new_client(self, username, client_socket):
        """
        Checks if the username received from the client is valid or not.
        :param username: A string representing the username of the client.
        :param client_socket: The clients socket.
        """
        if username not in self.client_message_dict and not username.startswith(
                '@') and ' ' not in username and username.lower() != 'server':
            self.pending_client_sockets.remove(client_socket)
            self.open_client_sockets.append(client_socket)
            self.client_message_dict[username] = (client_socket, [])
            # Check if this username is an admin username
            if username.lower() in self.admin_usernames:
                client_socket.send('ADMIN' + username)
            else:
                client_socket.send(username)
            print 'Added client socket {} to dict'.format(username)
        else:
            client_socket.send('False')

    def read_from_client(self):
        """
        Reads data from client
        """
        for client_socket in self.rlist:
            if client_socket is self.server_socket:
                # A new client has connected
                (new_socket, address) = self.server_socket.accept()
                # Add the new client_socket to the dictionary with an empty messages list,
                # to the open_client_sockets_list and to the pending_client_sockets list.
                self.pending_client_sockets.append(new_socket)
            else:
                # There is some data to be received
                data = client_socket.recv(1024)
                if data == '':
                    username_list = [username for username in self.client_message_dict if
                                     self.client_message_dict[username][0] == client_socket]
                    if username_list:
                        username = username_list[0]
                        self.disconnect_client(username, 'has left the chat')
                    else:
                        # Close connection
                        client_socket.close()
                        # The client has disconnected
                        self.pending_client_sockets.remove(client_socket)
                        print 'Connection with unregistered client closed'
                elif data.startswith('VALIDATE'):
                    self.validate_new_client(data.split('VALIDATE')[1], client_socket)
                else:
                    self.do_commands(data)

    def send_waiting_messages(self):
        """
        Sends waiting messages that need to be sent, only if the client's socket is writable and the client is not
        pending.
        """
        for client_tuple in self.client_message_dict.values():
            client_socket = client_tuple[0]
            if client_socket in self.wlist and client_socket in self.open_client_sockets:
                messages_list = client_tuple[1]
                for message in messages_list:
                    data = message
                    client_socket.send(data)
                    messages_list.remove(message)


def main():
    server = Server()
    while True:
        server.update_sockets_status()
        server.read_from_client()
        server.send_waiting_messages()


if __name__ == '__main__':
    main()
