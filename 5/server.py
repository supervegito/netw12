import select
import socket

server_socket = socket.socket()
server_socket.bind(('0.0.0.0', 23))
server_socket.listen(5)
open_client_sockets = []
""" :type A list of all connected client sockets """
client_message_dict = {}
""" :type A dictionary of tuples {SocketObject: (socket_address, [messages_to_be_sent])} """


def read_from_client(rlist):
    """
    Reads data from client
    :param rlist(list) contains all the sockets that can be read from
    """
    for socket in rlist:
        if socket is server_socket:
            # A new client has connected
            (new_socket, address) = server_socket.accept()
            open_client_sockets.append(new_socket)
            # Add the new socket to the dictionary with an empty messages list
            client_message_dict[new_socket] = (address, [])
        else:
            # There is some data to be received
            data = socket.recv(1024)
            if data == '':
                # The client disconnected
                print 'Connection with client {} closed'.format(client_message_dict[socket][0])
                # Remove the client and it's messages from the dictionary and from the open_client_sockets_list
                del client_message_dict[socket]
                open_client_sockets.remove(socket)
            else:
                # Append the client's message to all the other clients' messages list
                for client_socket in client_message_dict:
                    if client_socket != socket:
                        address = client_message_dict[socket][0]
                        client_message_dict[client_socket][1].append(str(address) + ': ' + data)


def send_waiting_messages(wlist):
    """
    Sends waiting messages that need to be sent, only if the client's socket is writable.
    :param wlist(list) contains all the sockets which can be writen to
    """
    for client_socket in client_message_dict:
        if client_socket in wlist:
            messages_list = client_message_dict[client_socket][1]
            for message in messages_list:
                data = message
                client_socket.send(data)
                messages_list.remove(message)


def main():
    while True:
        rlist, wlist, xlist = select.select([server_socket] + open_client_sockets, open_client_sockets, [])
        read_from_client(rlist)
        send_waiting_messages(wlist)


if __name__ == '__main__':
    main()
