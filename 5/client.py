import socket
import select
from modules import msvcrt

my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
my_socket.connect(('127.0.0.1', 23))
messages_to_send = []
print '\nWelcome to iChat!!!\nEach time you want to type something, enter the message and press the enter key at the ' \
      'end.\n'


def handle_socket():
    """
    Handles the socket logic
    """
    rlist, wlist, xlist = select.select([my_socket], [my_socket], [])
    if my_socket in rlist:
        # Read data from server
        data = my_socket.recv(1024)
        if data:
            print data
        else:
            # Server sent no data, close connection and exit the program
            print 'Connection with server closed'
            exit(1)
    if my_socket in wlist:
        # Send all waiting messages to server
        for msg in messages_to_send:
            my_socket.send(msg)
            messages_to_send.remove(msg)


def get_input():
    """
    Gets user input
    """
    message = msvcrt.getch()
    if message:
        messages_to_send.append(message)


def main():
    """
    Handles the client logic
    """
    while True:
        while not msvcrt.kbhit():
            # There is no keyboard event happening
            handle_socket()

        while msvcrt.kbhit():
            # There is a keyboard event happening
            get_input()


if __name__ == '__main__':
    main()
