#! /usr/bin/env python
#  -*- coding: utf-8 -*-
#
# Support module generated by PAGE version 4.25.1
#  in conjunction with Tcl version 8.6
#    Oct 09, 2019 04:20:58 PM IDT  platform: Darwin
#    Oct 09, 2019 04:29:32 PM IDT  platform: Darwin


import tkMessageBox

try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk

    py3 = False
except ImportError:
    import tkinter.ttk as ttk

    py3 = True


def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top
    global client, commands_list, alive
    alive = True
    client = kwargs['client']
    w.UsernameLabel.configure(text='''username: ''' + client.username)
    root.update()
    help()
    commands_list = ['Send Normal Message', 'Send Private Message', 'Remove User', 'Mute User', 'Unmute User',
                     'Make User Admin', "Remove Admin Permissions"]


def destroy_window():
    if tkMessageBox.askyesno('Warning', 'Are you sure you want to exit iCht?'):
        # Clear the client's messages_to_send list so it won't send the server messages after it's already gone.
        client.messages_to_send = []
        # Function which closes the window.
        global top_level
        top_level.destroy()
        top_level = None
        global alive
        alive = False


def set_Tk_var():
    global commands_combobox, users_combobox
    commands_combobox = tk.StringVar()
    commands_combobox.set('Pick A Command')
    users_combobox = tk.StringVar()
    users_combobox.set('Pick a user')


def configure_commands_combobox_values():
    """ Configures the right values for the commands combobox """
    if client.admin:
        w.CommandsTCombobox.configure(values=commands_list)
    else:
        w.CommandsTCombobox.configure(values=commands_list[:2])


def configure_users_combobox_values():
    """ Configures the right values for the users combobox """
    if w.CommandsTCombobox.current() > 0:
        users_list = client.users_list
        if not users_list:
            w.UsersTCombobox.set('Pick a user')
        w.UsersTCombobox.configure(values=users_list)
    else:
        w.UsersTCombobox.configure(values=[])
        w.UsersTCombobox.set('Pick a user')


def insert_messages(messages):
    if messages[6:].startswith('@SERVER: \n- '):
        alert('Info', 'Admins:' + messages[15:])
    elif messages[6:].startswith('@SERVER: Command rejected!'):
        alert('Error', messages[15:])
    else:
        w.MessagesScrolledtext.configure(state='normal')
        w.MessagesScrolledtext.insert(tk.END, messages + '\n')
        w.MessagesScrolledtext.configure(state='disabled')


def alert(type, message):
    if type == 'Info':
        tkMessageBox.showinfo(type, message)
    elif type == 'Warning':
        tkMessageBox.showwarning(type, message)
    else:
        tkMessageBox.showerror(type, message)


def send(command_number=None):
    print('Send button was clicked!')
    if not command_number:
        command_number = w.CommandsTCombobox.current()
    message = w.CommandScrolledText.get('1.0', tk.END).rstrip()
    if command_number == -1:
        alert('Error', 'You must choose a command!')
    elif command_number == 0:
        if message:
            client.append_message(str(command_number + 1) + message)
        else:
            alert('Error', 'You cannot send a blank message!')
    elif command_number == 1:
        user = w.UsersTCombobox.get()
        if message:
            client.append_message(str(command_number + 1) + user + ' ' + message)
        else:
            alert('Error', 'You cannot send a blank message!')
    elif command_number < 8:
        user = w.UsersTCombobox.get()
        client.append_message(str(command_number + 1) + user)
    else:
        client.append_message(str(command_number + 1) + ' ')

    # Clear text
    w.CommandScrolledText.delete('1.0', tk.END)


def view_admins():
    print('View Admins button was clicked!')
    send(command_number=8)


def exit_():
    print('Exit button was clicked!')
    destroy_window()


def help():
    print('Help button was clicked!')
    alert('Info', client.get_help_message())


def mainloop():
    while alive:
        received_messages = client.handle_socket()
        configure_commands_combobox_values()
        configure_users_combobox_values()
        if received_messages:
            insert_messages(received_messages)
        root.update()


if __name__ == '__main__':
    import main

    main.vp_start_gui()
