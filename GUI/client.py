import socket
import select
import os
import json
import ast


class Client(object):

    @staticmethod
    def clear_terminal():
        os.system("clear")

    def __init__(self):
        # Clear the terminal
        Client.clear_terminal()
        self.my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.my_socket.connect(('127.0.0.1', 23))
        self.messages_to_send = []
        """ :param A list containing the messages that are needed to be sent. """
        self.admin = False
        """ :param A boolean representing whether this client is and admin user or not. """
        self.rlist, self.wlist, self.xlist = select.select([self.my_socket], [self.my_socket], [])
        """ :param Lists representing the sockets you can read from, write to and error sockets. """
        self.username = ''
        """ :param A string representing the username of the client. """
        self.users_list = []
        """ :param A list of all active users. """

    @staticmethod
    def get_help_message():
        """
        Prints the a message about how to use the chat and the commands that exist. The message changes depending on
        whether you're an admin or not.
        """
        apple_logo = u'\uf8ff'
        welcome_line = '\nWelcome to iCh' + apple_logo + 't!!!\n'
        return welcome_line + '>Select a command from the commands drop down menu.\n>Select a user if needed.\n>' \
                              'Press the Send button to activate the command.\n>To view the admins press the "View ' \
                              'Admins" button.\n>To exit the chat press the "Exit" button or the "x" button in the ' \
                              'top left.\n>To see this message again press the "Help" button.'

    def make_username(self, username):
        """
        Asks the user for a username and sends it to the server to get validation.
        :param username: The requested username of the client.
        """
        self.my_socket.send('VALIDATE' + username)
        response = self.my_socket.recv(1024)
        if response != 'False':
            # The validation was successful
            if response.startswith('ADMIN'):
                # Logged in as an admin user
                self.admin = True
                username = response.split('ADMIN')[1]
                self.username = username
                self.users_list = ast.literal_eval(self.my_socket.recv(1024).split('list')[1])
                self.users_list.remove(self.username)
                return '>You have successfully created an admin user!\n'
            else:
                # Logged in as a regular user
                self.username = username
                users_list = self.my_socket.recv(1024).split('Users list')[1]
                self.users_list = ast.literal_eval(users_list)
                self.users_list.remove(self.username)
                return '>You have successfully created a user!\n'
        else:
            return '>It seems you have entered an invalid username.\n>Note that usernames cannot be "server", ' \
                   'cannot start with a "@", cannot include white spaces and must be unique!\n'

    def send_messages(self):
        """
        Send all waiting messages to server.
        """
        for msg in self.messages_to_send:
            # Get the command number
            command_number = int(msg[0])
            # Everything else is additional command parameters
            params = msg[1:]
            # Send username + command number + additional parameters
            data = {'username': self.username, 'command_number': command_number, 'params': params}
            json_data = json.dumps(data)
            self.my_socket.send(json_data)
            self.messages_to_send.remove(msg)

    @staticmethod
    def exit_program():
        """
        Prints a message and exits the program.
        """
        print '\rConnection with server closed'
        exit(1)

    def update_socket_status(self):
        """
        Updates the socket's status.
        """
        self.rlist, self.wlist, self.xlist = select.select([self.my_socket], [self.my_socket], [])

    def try_to_read_from_server(self):
        """
        Tries to read data from server and handles the received data.
        """
        if self.my_socket in self.rlist:
            # Read data from server
            data = self.my_socket.recv(1024)
            if data:
                if data[6:].startswith('@SERVER: Administration granted!'):
                    # It is a message from the server that makes the client admin
                    self.admin = True
                    return data + '\n' + self.get_help_message()
                elif data[6:].startswith('@SERVER: Administration removed!'):
                    # It is a message from the server that makes the client a regular user
                    self.admin = False
                    return data + '\n' + self.get_help_message()
                elif data[6:].startswith('@SERVER: Users list'):
                    self.users_list = ast.literal_eval(data[25:])
                    self.users_list.remove(self.username)
                else:
                    return data
            else:
                self.exit_program()

    def try_to_write_to_server(self):
        """
        Tries to write data to server.
        """
        if self.my_socket in self.wlist:
            self.send_messages()

    def handle_socket(self):
        """
        Handles the socket logic
        """
        self.update_socket_status()
        received_messages = self.try_to_read_from_server()
        self.try_to_write_to_server()
        return received_messages

    def append_message(self, message):
        """
        Appends client's message to the messages needed to be sent list.
        """
        self.messages_to_send.append(message)
